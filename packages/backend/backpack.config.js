module.exports = {
  webpack: config => {
    config.module.rules = [
      {
        exclude: /node_modules/,
        test: /\.graphql$/i,
        use: [{ loader: 'graphql-import-loader' }],
      },
      ...config.module.rules,
    ]
    return config
  },
}
