import { request, GraphQLClient } from 'graphql-request'
import jwt from 'jsonwebtoken'

const AUTH_SIGNUP = `mutation AUTH_SIGNUP($email: String! $password: String! $name: String!) {
  signup(email: $email password: $password name: $name) {
    email
    scopes
  }
}`
const AUTH_LOGIN = `mutation AUTH_LOGIN($email: String! $password: String!) {
  login(email: $email password: $password) {
    name
    email
    scopes
  }
}`

describe('Authentication', () => {
  it('Should allow to sign up', () => {
    const email = `john.doe-${Date.now()}@example.com`
    const variables = {
      email,
      password: 'Pa$$w0rd',
      name: 'John Doe',
    }
    expect(
      request(process.env.GRAPHQL_ENDPOINT, AUTH_SIGNUP, variables),
    ).resolves.toEqual({
      signup: {
        email,
        scopes: ['article:publish'],
      },
    })
  })

  it('Should allow to log in', async () => {
    const variables = {
      email: 'hazel.mckinney24@example.com',
      password: 'kathleen',
    }

    expect(
      await request(process.env.GRAPHQL_ENDPOINT, AUTH_LOGIN, variables),
    ).toMatchSnapshot()
  })

  describe('With an existing session', () => {
    const token = jwt.sign(
      {
        email: 'hazel.mckinney24@example.com',
        scopes: ['article:publish'],
      },
      process.env.APP_SECRET,
    )
    const client = new GraphQLClient(process.env.GRAPHQL_ENDPOINT, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })

    it('Should fail to sign up', () => {
      const variables = {
        email: 'john.doe@example.com',
        password: 'Pa$$w0rd',
        name: 'John Doe',
      }

      expect(client.request(AUTH_SIGNUP, variables)).rejects.toThrow(
        'Not Authorised',
      )
    })

    it('Should fail to log in', () => {
      const variables = {
        email: 'hazel.mckinney24@example.com',
        password: 'kathleen',
      }

      expect(client.request(AUTH_LOGIN, variables)).rejects.toThrow(
        'Not Authorised',
      )
    })
  })
})
