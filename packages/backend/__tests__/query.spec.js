import { request, GraphQLClient } from 'graphql-request'
import jwt from 'jsonwebtoken'

const QUERY_FEED = `query QUERY_FEED {
  feed {
    published
  }
}`
const QUERY_DRAFTS = `query QUERY_DRAFTS {
  drafts {
    published
  }
}`
const QUERY_POST = `query QUERY_POST($slug: String!) {
  article(slug: $slug) {
    published
    title
    slug
    body
    cover
    tags
    author {
      name
      bio
      image
      email
    }
  }
}`
const QUERY_ME = `query QUERY_ME {
  me {
    name
    bio
    image
    email
    scopes
  }
}`
const QUERY_PROFILE = `query QUERY_PROFILE($email: String!) {
  profile(email: $email) {
    name
    email
    image
    bio
    posts {
      published
      title
      slug
      body
      cover
      tags
      author {
        name
        email
        image
      }
    }
  }
}`

let token
let client

beforeAll(() => {
  token = jwt.sign(
    {
      email: 'sandra.myers73@example.com',
      scopes: ['article:publish'],
    },
    process.env.APP_SECRET,
  )
  client = new GraphQLClient(process.env.GRAPHQL_ENDPOINT, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  })
})

describe('Feed', () => {
  it('Should list published articles', async () => {
    const { feed: data } = await request(
      process.env.GRAPHQL_ENDPOINT,
      QUERY_FEED,
    )

    expect(Array.isArray(data)).toBeTruthy()
    data.forEach(article => expect(article.published).toBe(true))
  })
})

describe('Drafts', () => {
  it('Should list unpublished articles', async () => {
    const { drafts: data } = await client.request(QUERY_DRAFTS)

    expect(Array.isArray(data)).toBeTruthy()
    data.forEach(article => expect(article.published).toBe(false))
  })
})

describe('Post', () => {
  it('Should show a post', async () => {
    const variables = {
      slug: 'godfather-ipsum',
    }
    const data = await request(
      process.env.GRAPHQL_ENDPOINT,
      QUERY_POST,
      variables,
    )

    expect(data).toMatchSnapshot()
  })
})

describe('Me', () => {
  it('Should show information of logged user', async () => {
    expect(await client.request(QUERY_ME)).toMatchSnapshot()
  })

  it('Shoul return null for an unauthenticared user', () => {
    expect(
      request(process.env.GRAPHQL_ENDPOINT, QUERY_ME),
    ).resolves.toMatchObject({ me: null })
  })
})

describe('Profile', () => {
  it('Should show profile of logged user', async () => {
    expect(
      await client.request(QUERY_PROFILE, {
        email: 'hazel.mckinney24@example.com',
      }),
    ).toMatchSnapshot()
  })

  it('Should not include drafts whether is not author', async () => {
    const { profile: data } = await request(
      process.env.GRAPHQL_ENDPOINT,
      QUERY_PROFILE,
      { email: 'sandra.myers73@example.com' },
    )
    data.posts.forEach(article => expect(article.published).toBe(true))
  })
})
