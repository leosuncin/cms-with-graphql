import { request, GraphQLClient } from 'graphql-request'
import jwt from 'jsonwebtoken'

const CREATE_DRAFT = `mutation CREATE_DRAFT($data: DraftInput!) {
  createDraft(data: $data) {
    slug
    title
    published
    tags
  }
}`
const PUBLISH_DRAFT = `mutation PUBLISH_DRAFT($slug: String!) {
  publish(slug: $slug) {
    slug
    published
  }
}`
const DELETE_DRAFT = `mutation DELETE_DRAFT($slug: String!) {
  deleteArticle(slug: $slug) {
    slug
  }
}`
const COMMENT_ARTICLE = `mutation COMMENT_ARTICLE($slug: String! $body: String!) {
  commentArticle(slug: $slug body: $body) {
    author {
      email
    }
  }
}`
const UPDATE_ARTICLE = `mutation UPDATE_ARTICLE($slug: String, $data: DraftInput) {
  updateArticle(slug: $slug data: $data) {
    title
    slug
    body
  }
}`

describe('Article', () => {
  const token = jwt.sign(
    {
      email: 'hazel.mckinney24@example.com',
      scopes: ['article:publish'],
    },
    process.env.APP_SECRET,
  )
  const client = new GraphQLClient(process.env.GRAPHQL_ENDPOINT, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  })

  describe('Create draft', () => {
    const variables = {
      data: {
        title: `Lorem Ipsum ${Date.now()}`,
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pulvinar bibendum tempus. Donec semper ex eu augue lacinia lobortis. Nam id condimentum nulla, ac porta ipsum. Phasellus malesuada nulla leo, at auctor quam lacinia ac. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer eu est faucibus, rhoncus magna eget, egestas libero. Curabitur in sollicitudin nulla.

Mauris sit amet elit eu erat sagittis viverra non eget libero. Donec id venenatis lorem, id tincidunt urna. Phasellus ut fermentum mi, eget bibendum tortor. Mauris ut augue ut augue elementum placerat vitae ut turpis. Nulla facilisis justo nisi, ut maximus nisi tincidunt nec. Quisque ac lectus ac ex commodo pulvinar. Aliquam erat volutpat. Integer sit amet erat non neque tincidunt tristique ac gravida dolor. Sed eros risus, tristique at dolor accumsan, ullamcorper interdum ligula.`,
        tags: {
          set: ['lorem', 'test'],
        },
      },
    }

    it('Should fail when not authenticated', () => {
      expect(
        request(process.env.GRAPHQL_ENDPOINT, CREATE_DRAFT, variables),
      ).rejects.toThrow('User not authenticated.')
    })

    it('Should be successfully', async () => {
      const expected = {
        createDraft: {
          slug: expect.stringMatching(/^lorem-ipsum-\d+$/),
          title: expect.stringMatching(/^Lorem Ipsum \d+$/),
          published: false,
          tags: expect.arrayContaining(['lorem', 'test']),
        },
      }

      const data = await client.request(CREATE_DRAFT, variables)

      expect(data).toMatchObject(expected)
    })
  })

  describe('Publish an article', () => {
    const variables = {
      slug: 'leave-the-gun',
    }

    it('Should fail when not authenticated', () => {
      expect(
        request(process.env.GRAPHQL_ENDPOINT, PUBLISH_DRAFT, variables),
      ).rejects.toThrow('User not authenticated.')
    })

    it('Should be successfully', async () => {
      const expected = {
        publish: {
          slug: variables.slug,
          published: true,
        },
      }
      const data = await client.request(PUBLISH_DRAFT, variables)

      expect(data).toMatchObject(expected)
    })

    it('Should fail when not own the article', () => {
      expect(
        client.request(PUBLISH_DRAFT, { slug: 'cup-whipped-crema-espresso' }),
      ).rejects.toThrow("User isn't the author of article.")
    })
  })

  describe('Delete an article', () => {
    let slug

    beforeAll(async () => {
      const variables = {
        data: {
          title: `Delete me #${Date.now()}`,
          body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        },
      }

      const { createDraft: data } = await client.request(
        CREATE_DRAFT,
        variables,
      )
      slug = data.slug
    })

    it('Should fail when not authenticated', () => {
      expect(
        request(process.env.GRAPHQL_ENDPOINT, DELETE_DRAFT, { slug }),
      ).rejects.toThrow('User not authenticated.')
    })

    it('Should be successfully', () => {
      expect(client.request(DELETE_DRAFT, { slug })).resolves.toMatchObject({
        deleteArticle: { slug },
      })
    })

    it('Should fail when not own the article', () => {
      expect(
        client.request(DELETE_DRAFT, { slug: 'cup-whipped-crema-espresso' }),
      ).rejects.toThrow("User isn't the author of article.")
    })
  })

  describe('Comment an article', () => {
    const variables = {
      slug: 'godfather-ipsum',
      body: `Mauris sit amet elit eu erat sagittis #${Date.now()} non eget libero.`,
    }

    it('Should fail when not authenticated', () => {
      expect(
        request(process.env.GRAPHQL_ENDPOINT, COMMENT_ARTICLE, variables),
      ).rejects.toThrow('User not authenticated.')
    })

    it('Should be successfully', async () => {
      const expected = {
        commentArticle: {
          author: {
            email: expect.stringMatching(/.*@example.com$/),
          },
        },
      }
      const data = await client.request(COMMENT_ARTICLE, variables)

      expect(data).toMatchObject(expected)
    })
  })

  describe('Update an article', () => {
    let slug
    const variables = {
      data: {
        title: `Lorem Ipsum Dolor sit ${Date.now()} Amet`,
        body:
          'Mauris sit amet elit eu erat sagittis viverra non eget libero. Donec id venenatis lorem, id tincidunt urna. Phasellus ut fermentum mi, eget bibendum tortor. Mauris ut augue ut augue elementum placerat vitae ut turpis. Nulla facilisis justo nisi, ut maximus nisi tincidunt nec. Quisque ac lectus ac ex commodo pulvinar. Aliquam erat volutpat. Integer sit amet erat non neque tincidunt tristique ac gravida dolor. Sed eros risus, tristique at dolor accumsan, ullamcorper interdum ligula.',
      },
    }

    beforeEach(async () => {
      const variables = {
        data: {
          title: `Lorem Ipsum Dolor ${Date.now()}`,
          body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pulvinar bibendum tempus. Donec semper ex eu augue lacinia lobortis. Nam id condimentum nulla, ac porta ipsum. Phasellus malesuada nulla leo, at auctor quam lacinia ac. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer eu est faucibus, rhoncus magna eget, egestas libero. Curabitur in sollicitudin nulla.`,
          tags: {
            set: ['lorem', 'test'],
          },
        },
      }
      const { createDraft: data } = await client.request(
        CREATE_DRAFT,
        variables,
      )
      slug = data.slug
    })

    it('Should fail when not authenticated', () => {
      expect(
        request(process.env.GRAPHQL_ENDPOINT, UPDATE_ARTICLE, {
          slug,
          ...variables,
        }),
      ).rejects.toThrow('User not authenticated.')
    })

    it('Should be successfully', async () => {
      const expected = {
        updateArticle: {
          title: expect.stringMatching(/Lorem Ipsum Dolor sit \d+ Amet/),
          slug,
          body: expect.stringMatching(/^Mauris.*/),
        },
      }

      const data = await client.request(UPDATE_ARTICLE, {
        slug,
        ...variables,
      })

      expect(data).toMatchObject(expected)
    })

    it('Should fail when not own the article', () => {
      expect(
        client.request(UPDATE_ARTICLE, {
          slug: 'epic-cheeseburgers',
          ...variables,
        }),
      ).rejects.toThrow("User isn't the author of article.")
    })
  })
})
