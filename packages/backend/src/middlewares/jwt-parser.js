import jwt from 'jsonwebtoken'

export default (req, res, next) => {
  const { authorization = '' } = req.headers
  const token = authorization.split(' ')[1] || req.cookies.token

  if (token) {
    const { iat, ...user } = jwt.verify(token, process.env.APP_SECRET)
    req.user = user
  }

  return next()
}
