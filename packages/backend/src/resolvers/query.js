import { PaginationError } from '../constants/errors'

export default {
  feed(parent, args, ctx, info) {
    const { page = 1, limit = 5, orderBy = 'published_DESC' } = args

    if (page < 1)
      throw new PaginationError({
        message: 'The page value must be greater than 1',
      })

    if (limit < 1)
      throw new PaginationError({
        message: 'The limit value must be greater than 1',
      })

    const skip = (page - 1) * limit

    return ctx.db.query.articles(
      {
        where: { ...args.where, published: true },
        orderBy,
        skip,
        first: limit,
      },
      info,
    )
  },

  drafts(parent, args, { db, request }, info) {
    const { page = 1, limit = 5, orderBy = 'published_DESC' } = args

    if (page < 1)
      throw new PaginationError({
        message: 'The page value must be greater than 1',
      })

    if (limit < 1)
      throw new PaginationError({
        message: 'The limit value must be greater than 1',
      })

    const skip = (page - 1) * limit

    return db.query.articles(
      {
        where: {
          ...args.where,
          published: false,
          author: { email: request.user.email },
        },
        orderBy,
        skip,
        first: limit,
      },
      info,
    )
  },

  article(parent, { slug }, ctx, info) {
    return ctx.db.query.article({ where: { slug } }, info)
  },

  me(parent, _, { db, request }, info) {
    if (!request.user) return null
    return db.query.user({ where: { email: request.user.email } }, info)
  },

  async profile(parent, { email }, ctx, info) {
    const isAuthor = ctx.request.user && ctx.request.user.email === email
    const profile = await ctx.db.query.user({ where: { email } }, info)

    if (profile.posts && !isAuthor) {
      profile.posts = profile.posts.filter(article => article.published)
    }

    return profile
  },
}
