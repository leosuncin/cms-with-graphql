export default {
  feedSubscription: {
    async subscribe(parent, _, ctx) {
      return ctx.db.subscription
        .article({
          where: {
            mutation_in: ['CREATED', 'UPDATED'], // eslint-disable-line camelcase
          },
        })
        .node()
    },
    resolve: payload => payload,
  },
}
