import Mutation from './mutation'
import Subscription from './subscription'
import Query from './query'

export default {
  Mutation,
  Subscription,
  Query,
}
