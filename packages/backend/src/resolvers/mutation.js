import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import getSlug from 'speakingurl'
import { AuthenticationError } from '../constants/errors'

function hash(value) {
  const salt = bcrypt.genSaltSync()
  return bcrypt.hashSync(value, salt)
}

function prepareToken({ email, scopes }, ctx) {
  const token = jwt.sign({ email, scopes }, process.env.APP_SECRET)

  ctx.response.header('Authorization', `Bearer ${token}`)
  ctx.response.cookie('token', token, {
    httpOnly: true,
    maxAge: 1000 * 3600 * 24 * 30,
  })

  return token
}

export default {
  async signup(parent, args, ctx) {
    const password = hash(args.password)
    const user = await ctx.db.mutation.createUser(
      { data: { ...args, password, scopes: { set: ['article:publish'] } } },
      `{
      name
      email
      scopes
    }`,
    )

    prepareToken(user, ctx)

    return user
  },
  async login(parent, { email, password }, ctx) {
    const user = await ctx.db.query.user(
      { where: { email } },
      '{ name email password scopes }',
    )

    if (!user) throw new AuthenticationError({ data: { email: true } })

    if (!bcrypt.compareSync(password, user.password))
      throw new AuthenticationError()

    prepareToken(user, ctx)

    return user
  },
  signout(parent, _, ctx) {
    ctx.response.clearCookie('token')
    return true
  },
  async createDraft(parent, { data }, { db, request, storage }, info) {
    const slug = getSlug(data.title)
    const cover = await storage.saveFile(data.cover, 'article-covers', slug)

    return db.mutation.createArticle(
      {
        data: {
          ...data,
          slug,
          cover,
          author: { connect: { email: request.user.email } },
        },
      },
      info,
    )
  },
  async publish(parent, { slug }, { db }, info) {
    return db.mutation.updateArticle(
      { data: { published: true }, where: { slug } },
      info,
    )
  },
  async deleteArticle(parent, { slug }, { db }, info) {
    return db.mutation.deleteArticle({ where: { slug } }, info)
  },
  async commentArticle(parent, { slug, body }, { db, request }, info) {
    return db.mutation.updateArticle(
      {
        where: {
          slug,
        },
        data: {
          comments: {
            create: [
              {
                body,
                author: {
                  connect: {
                    email: request.user.email,
                  },
                },
              },
            ],
          },
        },
      },
      info,
    )
  },
  async updateArticle(parent, { slug, data }, { db }, info) {
    return db.mutation.updateArticle(
      {
        where: { slug },
        data,
      },
      info,
    )
  },
}
