import { Client as MinioClient } from 'minio'
import { generate as shortid } from 'shortid'

const client = new MinioClient({
  endPoint: process.env.MINIO_ENDPOINT,
  port: parseInt(process.env.MINIO_PORT, 10),
  useSSL: process.env.NODE_ENV === 'production',
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY,
})

const throwArgumentError = arg => {
  throw new Error(`${arg} is required`)
}

export async function saveFile(
  upload,
  bucketName = throwArgumentError('bucketName'),
  saveAs = shortid(),
) {
  if (Promise.resolve(upload) !== upload) return

  const { createReadStream, filename, mimetype } = await upload
  const existsBucket = await client.bucketExists(bucketName)
  const objectName = filename.replace(
    /.*(\.\w{3,4})/i,
    (_, match) => saveAs + match,
  )

  if (!existsBucket) await client.makeBucket(bucketName)

  await client.putObject(bucketName, objectName, createReadStream(), {
    'Content-Type': mimetype,
  })

  return `/assets/${bucketName}/${objectName}`
}

export async function streamFile(
  bucketName = throwArgumentError('bucketName'),
  objectName = throwArgumentError('objectName'),
) {
  return client.getObject(bucketName, objectName)
}
