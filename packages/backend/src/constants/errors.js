import { createError } from 'apollo-errors'

export const AuthorizationError = createError('AuthorizationError', {
  message: 'User not authenticated.',
})

export const ScopeError = createError('ScopeError', {
  message: "User doesn't have the required scope.",
})

export const AuthorError = createError('AuthorError', {
  message: "User isn't the author of article.",
})

export const ArticleNotFound = createError('ArticleNotFound', {
  message: 'Article not found',
})

export const AuthenticationError = createError('AuthenticationError', {
  message: 'Incorrect email or password',
})

export const PaginationError = createError('PaginationError', {
  message: 'Incorrect value for pagination argument',
})
