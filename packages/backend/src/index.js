import { GraphQLServer, PubSub } from 'graphql-yoga'
import cookieParser from 'cookie-parser'

import resolvers from './resolvers'
import typeDefs from './schema.graphql'
import permissions from './permissions'
import db from './db'
import * as storage from './services/storage'
import jwtParser from './middlewares/jwt-parser'

const pubsub = new PubSub()
const options = {
  resolvers,
  typeDefs,
  resolverValidationOptions: {
    requireResolversForResolveType: false,
  },
  middlewares: [permissions],
  context(req) {
    return { ...req, pubsub, db, storage }
  },
}

const server = new GraphQLServer(options)

server.express.use(cookieParser(process.env.APP_SECRET))
server.express.use(jwtParser)

server.get('/assets/:bucket/:filename', async (req, res) => {
  const { bucket, filename } = req.params
  try {
    const stream = await storage.streamFile(bucket, filename)
    stream.pipe(res)
  } catch (error) {
    console.error(error)
    res.status(503).send(error)
  }
})

server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.ALLOWED_ORIGINS.split(/\s*,\s*/),
    },
  },
  deets => {
    console.log(`Server is now running on port http://localhost:${deets.port}`)
  },
)
