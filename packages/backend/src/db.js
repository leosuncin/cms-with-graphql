import { Prisma } from 'prisma-binding'
import typeDefs from './prisma/generated.graphql'

const db = new Prisma({
  typeDefs,
  endpoint: process.env.PRISMA_ENDPOINT,
  secret: process.env.PRISMA_MANAGEMENT_API_SECRET,
})

export default db
