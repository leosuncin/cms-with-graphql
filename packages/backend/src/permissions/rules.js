import { rule } from 'graphql-shield'

import {
  AuthorizationError,
  ScopeError,
  AuthorError,
  ArticleNotFound,
} from '../constants/errors'

export const isAuthenticated = rule('isAuthenticated')((parent, _, ctx) => {
  return ctx.request.user ? true : new AuthorizationError()
})

export const canPublishArticle = rule('canPublishArticle', { cache: 'strict' })(
  async (parent, { slug }, ctx) => {
    const scope = 'article:publish'
    if (!ctx.request.user) return false

    const [user, article] = await Promise.all([
      ctx.db.query.user(
        { where: { email: ctx.request.user.email } },
        '{ email scopes }',
      ),
      ctx.db.query.article(
        {
          where: {
            slug,
          },
        },
        '{ author { email } }',
      ),
    ])
    ctx.request.user = user

    if (!article)
      throw new ArticleNotFound({
        data: {
          articleSlugRequested: slug,
        },
      })

    if (user.email !== article.author.email)
      return user.scopes.includes('user:admin') ? true : new AuthorError()

    return user.scopes.includes(scope) ? true : new ScopeError()
  },
)

export const isArticleAuthor = rule('isArticleAuthor')(
  async (parent, { slug }, ctx) => {
    if (!ctx.request.user) return false
    // eslint-disable-next-line new-cap
    const isAuthor = await ctx.db.exists.Article({
      slug,
      author: { email: ctx.request.user.email },
    })

    return isAuthor ? true : new AuthorError()
  },
)
