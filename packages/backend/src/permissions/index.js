import { shield, and, not } from 'graphql-shield'

import { isAuthenticated, canPublishArticle, isArticleAuthor } from './rules'

const permissions = shield(
  {
    Query: {
      drafts: isAuthenticated,
    },
    Mutation: {
      signup: not(isAuthenticated),
      login: not(isAuthenticated),
      createDraft: isAuthenticated,
      publish: and(isAuthenticated, canPublishArticle),
      deleteArticle: and(isAuthenticated, isArticleAuthor),
      commentArticle: isAuthenticated,
      updateArticle: and(isAuthenticated, isArticleAuthor),
    },
  },
  { debug: process.env.NODE_ENV !== 'production', allowExternalErrors: true },
)

export default permissions
