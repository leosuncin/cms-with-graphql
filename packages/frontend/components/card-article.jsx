import PropTypes from 'prop-types'
import Link from 'next/link'
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  CardSubtitle,
  CardFooter,
} from 'reactstrap'

const textSize = 200

const CardArticle = ({
  published,
  title,
  slug,
  body,
  cover,
  tags,
  publishedAt,
  createdAt,
  author,
}) => (
  <Card className="mb-4">
    <CardImg top width="100%" src={cover} />
    <CardBody>
      <CardTitle tag="h2">{title}</CardTitle>
      <CardSubtitle tag="small">
        <b>Tags: </b>
        {tags.map((tag, idx, self) => (
          <Link key={tag} href={`/?tag={tag}`}>
            <a>
              {tag}
              {idx < self.length - 1 ? ', ' : ''}
            </a>
          </Link>
        ))}
      </CardSubtitle>
      <CardText>
        {body.length < textSize ? body : `${body.substr(textSize)}...`}
      </CardText>
      <Link href={`/article?slug=${slug}`} as={`/article/${slug}`}>
        <a className="btn btn-primary">Read more &rarr;</a>
      </Link>
    </CardBody>
    <CardFooter className="text-muted">
      {published ? 'Posted' : 'Created'} on{' '}
      {(published ? publishedAt : createdAt).toLocaleDateString()} by{' '}
      <Link
        href={`/profile/?email=${author.email}`}
        as={`/profile/${author.email}`}
      >
        <a>{author.name}</a>
      </Link>
    </CardFooter>
  </Card>
)

export const articleShape = {
  published: PropTypes.bool,
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  cover: PropTypes.string,
  tags: PropTypes.arrayOf(PropTypes.string),
  publishedAt: PropTypes.instanceOf(Date),
  createdAt: PropTypes.instanceOf(Date),
  author: PropTypes.shape({
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    email: PropTypes.string.isRequired,
  }).isRequired,
}

CardArticle.propTypes = articleShape

CardArticle.defaultProps = {
  published: true,
  cover:
    'https://placeholdit.imgix.net/~text?txtsize=33&txt=750%C3%97300&w=750&h=300',
  tags: [],
  publishedAt: new Date(),
  createdAt: new Date(),
}

export default CardArticle
