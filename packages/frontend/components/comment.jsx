import PropTypes from 'prop-types'
import { Media } from 'reactstrap'

const Comment = ({ body, author }) => (
  <Media className="mb-4" tag="li">
    <Media left>
      <Media
        object
        className="mr-3 rounded-circle"
        src={author.image}
        width="64"
        alt={author.name}
      />
    </Media>
    <Media body>
      <Media heading>{author.name}</Media>
      {body}
    </Media>
  </Media>
)

Comment.propTypes = {
  body: PropTypes.string.isRequired,
  author: PropTypes.shape({
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    email: PropTypes.string.isRequired,
  }).isRequired,
}

export default Comment
