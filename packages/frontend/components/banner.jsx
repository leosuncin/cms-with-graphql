import Link from 'next/link'
import PropTypes from 'prop-types'

import './banner.scss'

const Banner = ({ title, description }) => (
  <section className="banner">
    <Link href="/">
      <a className="banner__logo" />
    </Link>
    <header>
      <h2>{title}</h2>
      <p>{description}</p>
    </header>
  </section>
)

Banner.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
}

export default Banner
