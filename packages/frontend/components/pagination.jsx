import Link from 'next/link'
import PropTypes from 'prop-types'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap'

const Pages = ({ isLastPage, isFirstPage, page = 1 }) => (
  <Pagination listClassName="justify-content-center mb-4">
    <PaginationItem disabled={isFirstPage}>
      <Link href={{ pathname: '/', query: { page: page - 1 } }}>
        <PaginationLink href={`/?page=${page - 1}`}>
          &larr; Newer
        </PaginationLink>
      </Link>
    </PaginationItem>
    <PaginationItem disabled={isLastPage}>
      <Link href={{ pathname: '/', query: { page: page + 1 } }}>
        <PaginationLink href={`/?page=${page + 1}`}>
        Older &rarr;
        </PaginationLink>
      </Link>
    </PaginationItem>
  </Pagination>
)

Pages.propTypes = {
  isFirstPage: PropTypes.bool,
  isLastPage: PropTypes.bool,
  page: PropTypes.number,
}

Pages.defaultProps = {
  isFirstPage: true,
  isLastPage: false,
  page: 1,
}

export default Pages
