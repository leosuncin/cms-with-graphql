import PropTypes from 'prop-types'
import { Card, CardBody, CardHeader, CardFooter } from 'reactstrap'

const CardQuote = ({ quote }) => (
  <Card>
    <CardHeader tag="h5">Quote of the day</CardHeader>
    <CardBody>
      <blockquote cite={quote.url}>{quote.body}</blockquote>
      <cite>{quote.author}</cite>
    </CardBody>
    <CardFooter>
      <span className="they-said-so">
        Powered by quotes from{' '}
        <a href="https://favqs.com" title="TheySaidSo Famous Quotes API">
          FavQs
        </a>
      </span>
    </CardFooter>
  </Card>
)

CardQuote.propTypes = {
  quote: PropTypes.object.isRequired,
}

export default CardQuote
