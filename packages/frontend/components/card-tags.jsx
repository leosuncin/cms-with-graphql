import PropTypes from 'prop-types'
import { Card, CardBody, CardHeader } from 'reactstrap'

import TagPill from './tag-pill'

const CardTags = ({ tags }) => (
  <Card className="my-4">
    <CardHeader tag="h5">Tags</CardHeader>
    <CardBody>
      {tags.map(tag => (
        <TagPill key={tag} tag={tag} />
      ))}
    </CardBody>
  </Card>
)

CardTags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
}

export default CardTags
