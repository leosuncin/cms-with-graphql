import Link from 'next/link'
import PropTypes from 'prop-types'

const TagPill = ({ tag, color }) => (
  <Link href={`/?tag=${tag}`}>
    <a className={`badge badge-${color} badge-pill`} style={{ margin: '2px' }}>
      {tag}
    </a>
  </Link>
)

TagPill.propTypes = {
  tag: PropTypes.string.isRequired,
  color: PropTypes.oneOf([
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'light',
    'dark',
  ]),
}

TagPill.defaultProps = {
  color: 'info',
}

export default TagPill
