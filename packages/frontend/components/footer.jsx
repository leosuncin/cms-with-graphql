import PropTypes from 'prop-types'

const year = new Date().getFullYear()

const Footer = props => (
  <footer className="py-5 bg-dark">
    <div className="container">
      <p className="m-0 text-center text-white">
        Copyright &copy; {props.title} {year}
      </p>
    </div>
  </footer>
)

Footer.propTypes = {
  title: PropTypes.string.isRequired,
}

export default Footer
