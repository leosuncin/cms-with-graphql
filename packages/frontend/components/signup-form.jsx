import Link from 'next/link'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import { Component } from 'react'

class SignUpForm extends Component {
  state = {
    name: '',
    email: '',
    password: '',
  }

  handleChange = event => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  handleSubmit = event => {
    event.preventDefault()
    return this.props.onSignup(this.state)
  }

  render() {
    const { loading } = this.props
    return (
      <Form name="signup" method="POST" onSubmit={this.handleSubmit}>
        <FormGroup tag="fieldset" disabled={loading} aria-busy={loading}>
          <Label for="signup-name">Name</Label>
          <Input
            required
            name="name"
            id="signup-name"
            value={this.state.name}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup tag="fieldset" disabled={loading} aria-busy={loading}>
          <Label for="signup-email">Email</Label>
          <Input
            required
            type="email"
            name="email"
            id="signup-email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup tag="fieldset" disabled={loading} aria-busy={loading}>
          <Label for="signup-password">Password</Label>
          <Input
            required
            type="password"
            name="password"
            id="signup-password"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </FormGroup>
        <Button
          block
          type="submit"
          color="primary"
          size="lg"
          disabled={loading}
        >
          Sign Up
        </Button>
        <Link href="/signin">
          <a className="btn btn-secondary btn-lg btn-block">Sign in</a>
        </Link>
      </Form>
    )
  }
}

SignUpForm.propTypes = {
  onSignup: PropTypes.func.isRequired,
  loading: PropTypes.bool,
}
SignUpForm.defaultProps = {
  loading: false,
}

export default SignUpForm
