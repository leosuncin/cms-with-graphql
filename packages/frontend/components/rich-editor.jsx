import PropTypes from 'prop-types'
import { Component } from 'react'
import RichTextEditor from 'react-rte'

class RichEditor extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    format: PropTypes.oneOf(['raw', 'html', 'markdown']),
  }

  static defaultProps = {
    value: null,
    format: 'markdown',
  }

  constructor(props) {
    super(props)
    const { value, format } = this.props
    this.state = {
      editorValue: value
        ? RichTextEditor.createValueFromString(value, format)
        : RichTextEditor.createEmptyValue(),
    }
  }

  handleChange = editorValue => {
    this.setState({ editorValue })
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(editorValue.toString(this.props.format))
    }
  }

  render() {
    return (
      <RichTextEditor
        className="border-0 h-auto"
        placeholder="Start writing here"
        value={this.state.editorValue}
        onChange={this.handleChange}
      />
    )
  }
}

export default RichEditor
