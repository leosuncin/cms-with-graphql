import PropTypes from 'prop-types'
import Link from 'next/link'
import { Component } from 'react'
import { Mutation, Query } from 'react-apollo'
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Media,
  Popover,
  PopoverBody,
  PopoverHeader,
  UncontrolledAlert,
} from 'reactstrap'

import QUERY_CURRENT_USER from '../graphql/QUERY_CURRENT_USER.graphql'
import QUERY_ARTICLE from '../graphql/QUERY_ARTICLE.graphql'
import MUTATION_COMMENT_ARTICLE from '../graphql/MUTATION_COMMENT_ARTICLE.graphql'
import Comment from './comment'

class Comments extends Component {
  constructor(props) {
    super(props)
    const { comments } = props
    this.state = {
      comments,
      body: '',
    }
  }

  handleChange = event => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  updateCache = (cache, result) => {
    const { slug } = this.props
    const data = cache.readQuery({
      query: QUERY_ARTICLE,
      variables: { slug },
    })
    const [newComment] = result.data.commentArticle.comments
    data.article.comments = [...data.article.comments, newComment]

    cache.writeQuery({ query: QUERY_ARTICLE, data, variables: { slug } })
  }

  render() {
    const { comments, _popoverOpen = false } = this.state
    const { slug } = this.props
    return (
      <>
        <Query query={QUERY_CURRENT_USER}>
          {({ data: { me } }) => (
            <Card className="my-4">
              <CardHeader tag="strong" id="form-title">
                Leave a Comment:
              </CardHeader>
              <CardBody>
                <Mutation
                  mutation={MUTATION_COMMENT_ARTICLE}
                  variables={{ slug, body: this.state.body }}
                  optimisticResponse={{
                    __typename: 'Mutation',
                    commentArticle: {
                      __typename: 'Article',
                      comments: [
                        {
                          __typename: 'Comment',
                          id: Date.now().toString(16),
                          body: this.state.body,
                          author: me
                            ? {
                                __typename: 'User',
                                name: me.name,
                                email: me.email,
                                image: me.image,
                              }
                            : {},
                        },
                      ],
                    },
                  }}
                  update={this.updateCache}
                >
                  {(commentArticle, { loading, error }) => (
                    <Form
                      method="POST"
                      name="commentArticle"
                      onSubmit={event => {
                        const { body } = this.state
                        const newComment = {
                          __typename: 'Comment',
                          id: Date.now().toString(16),
                          body,
                          author: {
                            name: me.name,
                            email: me.email,
                            image: me.image,
                            __typename: 'User',
                          },
                        }
                        event.preventDefault()
                        commentArticle()
                        this.setState(prevState => ({
                          comments: [...prevState.comments, newComment],
                          body: '',
                        }))
                      }}
                    >
                      <FormGroup>
                        <Input
                          required
                          type="textarea"
                          name="body"
                          rows="3"
                          aria-labelledby="form-title"
                          value={this.state.body}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                      <Button
                        id="submit-comment"
                        type="submit"
                        color="primary"
                        disabled={loading}
                      >
                        Add Comment
                      </Button>
                      <Popover
                        target="submit-comment"
                        placement="bottom"
                        trigger="click hover"
                        toggle={() => this.setState({ _popoverOpen: !me })}
                        isOpen={_popoverOpen}
                      >
                        <PopoverHeader>An account is needed</PopoverHeader>
                        <PopoverBody>
                          You need to{' '}
                          <Link href="/signin">
                            <a>sign in</a>
                          </Link>{' '}
                          before comment
                        </PopoverBody>
                      </Popover>
                      {error && (
                        <UncontrolledAlert color="danger">
                          <p>The following errors were found:</p>
                          <ul>
                            {error.graphQLErrors.map(err => (
                              <li key={err.message}>{err.message}</li>
                            ))}
                          </ul>
                        </UncontrolledAlert>
                      )}
                    </Form>
                  )}
                </Mutation>
              </CardBody>
            </Card>
          )}
        </Query>
        <Media list>
          {comments.map(comment => (
            <Comment key={comment.id} {...comment} />
          ))}
        </Media>
      </>
    )
  }
}

Comments.propTypes = {
  comments: PropTypes.array.isRequired,
  slug: PropTypes.string.isRequired,
}

export default Comments
