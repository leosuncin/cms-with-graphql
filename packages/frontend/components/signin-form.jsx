import Link from 'next/link'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import { Component } from 'react'

class SignInForm extends Component {
  state = {
    email: '',
    password: '',
  }

  handleChange = event => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  handleSubmit = event => {
    event.preventDefault()
    return this.props.onSignin(this.state)
  }

  render() {
    const { loading } = this.props
    return (
      <Form name="login" method="POST" onSubmit={this.handleSubmit}>
        <FormGroup tag="fieldset" disabled={loading} aria-busy={loading}>
          <Label for="login-email">Email</Label>
          <Input
            required
            type="email"
            name="email"
            id="login-email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup tag="fieldset" disabled={loading} aria-busy={loading}>
          <Label for="login-password">Password</Label>
          <Input
            required
            type="password"
            name="password"
            id="login-password"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </FormGroup>
        <Button
          block
          type="submit"
          color="primary"
          size="lg"
          disabled={loading}
        >
          Sign In
        </Button>
        <Link href="/signup">
          <a className="btn btn-secondary btn-lg btn-block">Sign up</a>
        </Link>
      </Form>
    )
  }
}

SignInForm.propTypes = {
  onSignin: PropTypes.func.isRequired,
  loading: PropTypes.bool,
}
SignInForm.defaultProps = {
  loading: false,
}

export default SignInForm
