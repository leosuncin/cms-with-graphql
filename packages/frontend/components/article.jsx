import Link from 'next/link'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'

import TagPill from './tag-pill'

const Article = ({
  published,
  title,
  slug,
  body,
  cover,
  tags,
  publishedAt,
  createdAt,
  author,
}) => (
  <article>
    <h1 className="mt-4">{title}</h1>
    <p className="lead">
      by{' '}
      <Link
        href={`/profile/?email=${author.email}`}
        as={`/profile/${author.email}`}
      >
        <a>{author.name}</a>
      </Link>
    </p>
    <hr />
    <small>
      {published ? 'Posted' : 'Created'} on{' '}
      {(published ? publishedAt : createdAt).toLocaleDateString()}
    </small>
    <hr />
    <img className="img-fluid rounded" src={cover} alt={slug} />
    <hr />
    <ReactMarkdown source={body} />
    <hr />
    <p>
      <b>Tags: </b>
      {tags.map(tag => (
        <TagPill key={tag} tag={tag} />
      ))}
    </p>
  </article>
)

Article.propTypes = {
  published: PropTypes.bool,
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  cover: PropTypes.string,
  tags: PropTypes.arrayOf(PropTypes.string),
  publishedAt: PropTypes.instanceOf(Date),
  createdAt: PropTypes.instanceOf(Date),
  author: PropTypes.shape({
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
    email: PropTypes.string.isRequired,
  }).isRequired,
}

Article.defaultProps = {
  published: true,
  cover:
    'https://placeholdit.imgix.net/~text?txtsize=33&txt=900%C3%97300&w=900&h=300',
  tags: [],
  publishedAt: new Date(),
  createdAt: new Date(),
}

export default Article
