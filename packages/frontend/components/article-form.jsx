import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { Component } from 'react'
import { Col, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import Dropzone from 'react-dropzone'
import TagsInput from 'react-tagsinput'

import './dropzone.scss'
import { ApolloError } from 'apollo-client'
import ShowAlertError from './show-alert-error'

const LazyRichEditor = dynamic(() => import('./rich-editor'), {
  ssr: false,
  loading: () => (
    <Input
      type="textarea"
      rows="3"
      name="body"
      placeholder="Start writing here"
    />
  ),
})

const addPreviewURL = event => {
  const files = [...(event.dataTransfer || event.target).files]

  files.forEach(file => {
    if (!/image\/.*/.test(file.type)) return
    Object.defineProperty(file, 'preview', { value: URL.createObjectURL(file) })
  })

  return files
}

class ArticleForm extends Component {
  state = {
    title: '',
    cover: '',
    body: '',
    tags: { set: [] },
  }

  handleSubmit = createDraft => event => {
    event.preventDefault()
    const { title, cover, body, tags } = this.state
    createDraft({
      variables: {
        data: { title, cover, body, tags },
      },
    })
  }

  handleDrop = acceptedFiles => {
    const cover = acceptedFiles[0]
    this.setState({ cover })
  }

  handleChangeEditor = body => {
    this.setState({ body })
  }

  handleChangeTitle = event => {
    this.setState({ title: event.target.value })
  }

  handleChangeTags = tags => {
    this.setState({ tags: { set: tags } })
  }

  render() {
    const { loading, error, createDraft } = this.props
    return (
      <Form
        name="draft"
        method="POST"
        encType="multipart/form-data"
        onSubmit={this.handleSubmit(createDraft)}
      >
        {error && (
          <FormGroup row>
            <ShowAlertError error={error} />
          </FormGroup>
        )}
        <FormGroup row>
          <Label sm="2" for="draft_title" className="text-right">
            Title
          </Label>
          <Col sm="10">
            <Input
              required
              id="draft_title"
              name="title"
              value={this.state.title}
              onChange={this.handleChangeTitle}
            />
          </Col>
        </FormGroup>
        <Dropzone
          name="cover"
          multiple={false}
          accept="image/*"
          getDataTransferItems={addPreviewURL}
          onDrop={this.handleDrop}
        >
          {({ getRootProps, getInputProps, isDragActive }) => {
            return (
              <div
                {...getRootProps()}
                className="dropzone d-flex justify-content-center align-items-center border rounded"
              >
                <input {...getInputProps()} />
                {isDragActive ? (
                  <p>Drop the cover image here...</p>
                ) : this.state.cover ? (
                  <img
                    src={this.state.cover.preview}
                    width="900px"
                    height="300px"
                  />
                ) : (
                  <p>
                    Try dropping the cover image here, or click to select image
                    to upload.
                  </p>
                )}
              </div>
            )
          }}
        </Dropzone>
        <FormGroup>
          <LazyRichEditor
            value={this.state.body}
            onChange={this.handleChangeEditor}
          />
          <hr />
        </FormGroup>
        <FormGroup row>
          <Label sm="2" className="text-right">
            Tags
          </Label>
          <Col sm="10">
            <TagsInput
              onlyUnique
              value={this.state.tags.set}
              addKeys={[9, 13, 188]}
              onChange={this.handleChangeTags}
            />
          </Col>
        </FormGroup>
        <Button
          type="submit"
          color="primary"
          size="lg"
          className="float-right mb-4"
          disabled={loading}
        >
          Save draft
        </Button>
      </Form>
    )
  }
}

ArticleForm.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.instanceOf(ApolloError),
  createDraft: PropTypes.func.isRequired,
}
ArticleForm.defaultProps = {
  error: null,
}

export default ArticleForm
