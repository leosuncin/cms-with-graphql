import {
  Card,
  CardBody,
  CardHeader,
  Form,
  FormGroup,
  Input,
  Button,
} from 'reactstrap'
import { Component } from 'react'

class CardSearch extends Component {
  handleSearch = event => {
    event.preventDefault()
    console.log(event.target.query.value)
  }

  render() {
    return (
      <Card className="my-4">
        <CardHeader tag="h5">Search</CardHeader>
        <CardBody>
          <Form name="feed" method="GET" onSubmit={this.handleSearch}>
            <FormGroup className="input-group">
              <Input
                required
                name="query"
                type="search"
                placeholder="Search for..."
                aria-label="Search input"
              />
              <span className="input-group-btn">
                <Button type="submit" color="secondary" aria-label="Find">
                  🔍
                </Button>
              </span>
            </FormGroup>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

export default CardSearch
