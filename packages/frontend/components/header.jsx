import Link from 'next/link'
import PropTypes from 'prop-types'
import {
  Collapse,
  Container,
  Nav,
  Navbar,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'
import { Mutation, Query } from 'react-apollo'
import { useState } from 'react'

import QUERY_CURRENT_USER from '../graphql/QUERY_CURRENT_USER.graphql'
import MUTATION_SIGNOUT from '../graphql/MUTATION_SIGNOUT.graphql'
import redirect from '../lib/redirect'

const Header = ({ title }) => {
  const [collapse, setCollapse] = useState(false)

  return (
    <Navbar dark color="dark" fixed="top" expand="lg">
      <Container>
        <Link href="/">
          <a className="navbar-brand">{title}</a>
        </Link>
        <NavbarToggler
          aria-label="Toggle navigation"
          onClick={() => setCollapse(!collapse)}
        />
        <Collapse navbar isOpen={collapse}>
          <Nav navbar className="ml-auto">
            <NavItem>
              <Link href="/">
                <a className="nav-link">Home</a>
              </Link>
            </NavItem>
            <Query query={QUERY_CURRENT_USER}>
              {({ loading, data: { me } }) =>
                loading || !me ? (
                  <NavItem active>
                    <Link href="/signin">
                      <a className="nav-link">Sign in</a>
                    </Link>
                  </NavItem>
                ) : (
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      {me.name}
                    </DropdownToggle>
                    <DropdownMenu right>
                      <Link
                        href={`/profile?email=${me.email}`}
                        as={`/profile/${me.email}`}
                      >
                        <DropdownItem tag="a" href={`/profile/${me.email}`}>
                          👤 Your profile
                        </DropdownItem>
                      </Link>
                      <Link href="/draft">
                        <DropdownItem tag="a" href="/draft">
                          📝 Create draft
                        </DropdownItem>
                      </Link>
                      <Mutation
                        mutation={MUTATION_SIGNOUT}
                        refetchQueries={[{ query: QUERY_CURRENT_USER }]}
                        onCompleted={() => redirect({}, '/')}
                      >
                        {(signout, _) => (
                          <DropdownItem onClick={signout}>
                            👋 Log out
                          </DropdownItem>
                        )}
                      </Mutation>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                )
              }
            </Query>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  )
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
}

export default Header
