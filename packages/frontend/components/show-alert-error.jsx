import PropTypes from 'prop-types'
import { UncontrolledAlert } from 'reactstrap'
import { ApolloError } from 'apollo-client'

const ShowAlerError = ({ error }) => (
  <UncontrolledAlert color="danger">
    <p>The following errors were found:</p>
    <ul>
      {error.graphQLErrors.map(err => (
        <li key={err.message}>{err.message}</li>
      ))}
    </ul>
  </UncontrolledAlert>
)

ShowAlerError.propTypes = {
  error: PropTypes.instanceOf(ApolloError).isRequired,
}

export default ShowAlerError
