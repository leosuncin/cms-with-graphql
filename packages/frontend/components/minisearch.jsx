import React, { Component } from 'react'

import './minisearch.scss'

class MiniSearch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visibility: false,
    }
    this.queryInput = React.createRef()
  }

  toggleVisibility = evt => {
    evt.preventDefault()
    const node = this.queryInput.current
    this.setState(
      prevState => ({
        visibility: !prevState.visibility,
      }),
      () => node.focus(),
    )
  }

  handleSearch = evt => {
    evt.preventDefault()
    console.log(evt.target.query.value)
  }

  render() {
    const { visibility } = this.state
    return (
      <span className={`minisearch ${visibility ? 'minisearch--visible' : ''}`}>
        <a href="#" title="Search" onClick={this.toggleVisibility}>
          🔍
        </a>
        <form
          className="minisearch__form"
          action="/"
          method="GET"
          onSubmit={this.handleSearch}
        >
          <input
            ref={this.queryInput}
            name="query"
            placeholder="Search"
            type="search"
          />
        </form>
      </span>
    )
  }
}

export default MiniSearch
