const withSass = require('@zeit/next-sass')
const { request } = require('graphql-request')

module.exports = withSass({
  target: process.env.BUILD_TARGET || 'server',
  webpack(config) {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    }

    return config
  },
  env: {
    GRAPHQL_ENDPOINT: process.env.GRAPHQL_ENDPOINT,
  },
  async exportPathMap() {
    const defaultRoutes = {
      '/': { page: '/' },
      '/draft': { page: '/draft' },
      '/signin': { page: '/signin' },
      '/signup': { page: '/signup' },
    }
    const { articles, users } = await request(
      process.env.PRISMA_ENDPOINT,
      `query {
        articles {
          slug
        }
        users {
          email
        }
      }`,
    )

    let routes = articles.reduce(
      (pages, { slug }) => ({
        ...pages,
        [`/article/${slug}`]: { page: '/article', query: { slug } },
      }),
      defaultRoutes,
    )
    routes = users.reduce(
      (pages, { email }) => ({
        ...pages,
        [`/profile/${email}`]: { page: '/profile', query: { email } },
      }),
      routes,
    )

    return routes
  },
})
