import Router from 'next/router'

/**
 * Redirect to
 *
 * @param {object} ctx Context
 * @param {string} target Redirect to
 */
export default function redirect(ctx = {}, target) {
  if (ctx && 'res' in ctx) {
    // Server
    // 303: "See other"
    ctx.res.writeHead(303, { Location: target })
    ctx.res.end()
  } else {
    // Browser
    Router.replace(target)
  }
}
