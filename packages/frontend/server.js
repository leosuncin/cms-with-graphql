const { createServer } = require('http')
const next = require('next')
const nextConfig = require('./next.config')
const routes = require('./routes')

const dev = process.env.NODE_ENV !== 'production'
const port = parseInt(process.env.PORT, 10) || 3000
const app = next({ dev, dir: __dirname, conf: nextConfig })
const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
  createServer(handler).listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
