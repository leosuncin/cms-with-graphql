import DraftLayout from '../layout/draft'

const DraftPage = props => <DraftLayout {...props} />

export default DraftPage
