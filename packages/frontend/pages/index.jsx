import fetch from 'isomorphic-unfetch'

import BlogLayout from '../layout/blog'
import QUERY_HOMEPAGE from '../graphql/QUERY_HOMEPAGE.graphql'

const ENDPOINT = 'https://favqs.com/api/qotd'
const Index = props => <BlogLayout {...props} />

Index.getInitialProps = async ({ apolloClient, query }) => {
  const page = parseInt(query.page, 10) || 1
  const limit = parseInt(query.limit, 10) || 5
  const { orderBy } = query
  const resp = await fetch(ENDPOINT)
  const { data: pageProps = {} } = await apolloClient.query({
    query: QUERY_HOMEPAGE,
    variables: {
      page,
      limit,
      orderBy,
    },
  })

  if (resp.status === 200) {
    const { quote } = await resp.json()

    return {
      ...pageProps,
      page,
      limit,
      orderBy,
      quote,
    }
  }

  return { ...pageProps, quote: {} }
}

export default Index
