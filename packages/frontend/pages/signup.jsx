import SignUpLayout from '../layout/signup'
import QUERY_CURRENT_USER from '../graphql/QUERY_CURRENT_USER.graphql'
import redirect from '../lib/redirect'

const SignUpPage = () => <SignUpLayout />

SignUpPage.getInitialProps = async ctx => {
  const { data } = await ctx.apolloClient.query({ query: QUERY_CURRENT_USER })

  if (data.me) {
    redirect(ctx, '/')
  }

  return {}
}

export default SignUpPage
