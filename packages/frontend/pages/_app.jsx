import App, { Container } from 'next/app'
import { ApolloProvider } from 'react-apollo'

import withApollo from '../lib/with-apollo'

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if ('getInitialProps' in Component) {
      pageProps = await Component.getInitialProps(ctx)
    }

    pageProps.query = ctx.query

    return { pageProps }
  }

  render() {
    const { Component, apollo, pageProps } = this.props

    return (
      <Container>
        <ApolloProvider client={apollo}>
          <Component {...pageProps} />
        </ApolloProvider>
      </Container>
    )
  }
}

export default withApollo(MyApp)
