import ArticleLayout from '../layout/article'
import QUERY_ARTICLE from '../graphql/QUERY_ARTICLE.graphql'

const ArticlePage = props => <ArticleLayout {...props} />

ArticlePage.getInitialProps = async ({ apolloClient, query }) => {
  const { data: pageProps = {} } = await apolloClient.query({
    query: QUERY_ARTICLE,
    variables: { slug: query.slug },
  })

  return pageProps
}

export default ArticlePage
