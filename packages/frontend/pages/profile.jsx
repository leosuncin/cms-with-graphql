import ProfileLayout from '../layout/profile'
import QUERY_PROFILE from '../graphql/QUERY_PROFILE.graphql'

const ProfilePage = props => <ProfileLayout {...props} />

ProfilePage.getInitialProps = async ({ apolloClient, query }) => {
  const { data: pageProps = {} } = await apolloClient.query({
    query: QUERY_PROFILE,
    variables: query,
  })

  return pageProps.profile || {}
}

export default ProfilePage
