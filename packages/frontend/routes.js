const routes = require('next-routes')

module.exports = routes()
  .add('article', '/article/:slug')
  .add('profile', '/profile/:email')
