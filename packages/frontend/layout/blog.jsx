import PropTypes from 'prop-types'
import { Col, Row } from 'reactstrap'

import CardArticle, { articleShape } from '../components/card-article'
import CardSearch from '../components/card-search'
import CardTags from '../components/card-tags'
import CardQuote from '../components/card-quote'
import Pagination from '../components/pagination'
import BaseLayout from './base'

const BlogLayout = props => (
  <BaseLayout>
    <Row>
      <Col md="8">
        {props.feed.map(post => (
          <CardArticle key={post.slug} {...post} />
        ))}
        <Pagination page={props.page} isFirstPage={props.page < 2} />
      </Col>
      <Col md="4">
        <CardSearch />
        <CardTags tags={['lorem', 'Godfather', 'Coffee']} />
        <CardQuote quote={props.quote} />
      </Col>
    </Row>
  </BaseLayout>
)

BlogLayout.propTypes = {
  quote: PropTypes.object.isRequired,
  feed: PropTypes.arrayOf(PropTypes.shape(articleShape)).isRequired,
  page: PropTypes.number,
}
BlogLayout.defaultProps = {
  page: 1,
}

export default BlogLayout
