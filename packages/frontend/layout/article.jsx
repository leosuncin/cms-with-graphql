import PropTypes from 'prop-types'
import { Col, Row } from 'reactstrap'
import Comments from '../components/comments'
import Article from '../components/article'
import CardSearch from '../components/card-search'
import CardTags from '../components/card-tags'
import BaseLayout from './base'

const ArticleLayout = ({ article }) => (
  <BaseLayout>
    <Row>
      <Col lg="8">
        <Article {...article} />
        <hr />
        <Comments comments={article.comments} slug={article.slug} />
      </Col>
      <Col md="4">
        <CardSearch />
        <CardTags tags={article.tags} />
      </Col>
    </Row>
  </BaseLayout>
)

const authorShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  image: PropTypes.string,
  email: PropTypes.string.isRequired,
})

ArticleLayout.propTypes = {
  article: PropTypes.shape({
    published: PropTypes.bool,
    title: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    cover: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    publishedAt: PropTypes.instanceOf(Date),
    createdAt: PropTypes.instanceOf(Date),
    author: authorShape.isRequired,
    comments: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
        author: authorShape.isRequired,
      }),
    ),
  }).isRequired,
}

export default ArticleLayout
