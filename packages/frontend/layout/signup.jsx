import { Col, Container, Row, UncontrolledAlert } from 'reactstrap'
import { Mutation } from 'react-apollo'

import SignUpForm from '../components/signup-form'
import MUTATION_SIGNUP from '../graphql/MUTATION_SIGNUP.graphql'
import QUERY_CURRENT_USER from '../graphql/QUERY_CURRENT_USER.graphql'
import redirect from '../lib/redirect'
import BaseLayout from './base'

const SignUpLayout = () => (
  <BaseLayout hideBanner>
    <Container className="py-5">
      <Row>
        <Col md="7" lg="6" className="mx-auto">
          <h1 className="mb-4">Register!</h1>
          <Mutation
            mutation={MUTATION_SIGNUP}
            refetchQueries={[{ query: QUERY_CURRENT_USER }]}
            onCompleted={() => redirect({}, '/')}
          >
            {(signup, { loading, error }) => (
              <>
                {error ? (
                  <UncontrolledAlert color="danger">
                    <p>The following errors were found:</p>
                    <ul>
                      {error.graphQLErrors.map(err => (
                        <li key={err.message}>{err.message}</li>
                      ))}
                    </ul>
                  </UncontrolledAlert>
                ) : null}
                <SignUpForm
                  loading={loading}
                  onSignup={variables => signup({ variables })}
                />
              </>
            )}
          </Mutation>
        </Col>
      </Row>
    </Container>
  </BaseLayout>
)

export default SignUpLayout
