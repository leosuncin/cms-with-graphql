import PropTypes from 'prop-types'
import { Container } from 'reactstrap'

import Header from '../components/header'
import Head from '../components/head'
import Banner from '../components/banner'
import Footer from '../components/footer'

import './base.scss'

const title = 'Future imperfect'
const BaseLayout = props => (
  <>
    <Head title={title} />
    <Header title={title} />
    {!props.hideBanner && (
      <Banner title={title} description="Another fine blog" />
    )}
    <Container>{props.children}</Container>
    <Footer title={title} />
  </>
)

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired,
  hideBanner: PropTypes.bool,
}
BaseLayout.defaultProps = {
  hideBanner: false,
}

export default BaseLayout
