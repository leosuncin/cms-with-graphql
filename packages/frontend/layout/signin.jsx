import Link from 'next/link'
import { Col, Container, Row, UncontrolledAlert } from 'reactstrap'
import { Mutation } from 'react-apollo'

import SignInForm from '../components/signin-form'
import MUTATION_SIGNIN from '../graphql/MUTATION_SIGNIN.graphql'
import QUERY_CURRENT_USER from '../graphql/QUERY_CURRENT_USER.graphql'
import redirect from '../lib/redirect'
import BaseLayout from './base'

const SignInLayout = () => (
  <BaseLayout hideBanner>
    <Container className="py-5">
      <Row>
        <Col md="7" lg="6" className="mx-auto">
          <h1 className="mb-4">Welcome back!</h1>
          <Mutation
            mutation={MUTATION_SIGNIN}
            refetchQueries={[{ query: QUERY_CURRENT_USER }]}
            onCompleted={() => redirect({}, '/')}
          >
            {(signin, { loading, error }) => (
              <>
                {error ? (
                  <UncontrolledAlert color="danger">
                    <p>The following errors were found:</p>
                    <ul>
                      {error.graphQLErrors.map(err => (
                        <li key={err.message}>{err.message}</li>
                      ))}
                    </ul>
                  </UncontrolledAlert>
                ) : null}
                <SignInForm
                  loading={loading}
                  onSignin={variables => signin({ variables })}
                />
              </>
            )}
          </Mutation>
          <div className="text-center py-2">
            <Link href="/recover-password">
              <a className="small">Forgot password?</a>
            </Link>
          </div>
        </Col>
      </Row>
    </Container>
  </BaseLayout>
)

export default SignInLayout
