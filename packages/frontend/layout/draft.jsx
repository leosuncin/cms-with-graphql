import { Col, Row } from 'reactstrap'
import { Mutation } from 'react-apollo'

import ArticleForm from '../components/article-form'
import MUTATION_CREATE_DRAFT from '../graphql/MUTATION_CREATE_DRAFT.graphql'
import QUERY_ARTICLE from '../graphql/QUERY_ARTICLE.graphql'
import redirect from '../lib/redirect'
import BaseLayout from './base'

const DraftLayout = () => (
  <BaseLayout>
    <Row>
      <Col lg="12">
        <h1 className="mb-4">Write a draft</h1>
        <Mutation
          mutation={MUTATION_CREATE_DRAFT}
          update={(cache, { data }) => {
            const { slug } = data.createDraft
            cache.writeQuery({
              query: QUERY_ARTICLE,
              variables: { slug },
              data: { ...data.createDraft, comments: [] },
            })
          }}
          onCompleted={({ createDraft: { slug } }) =>
            redirect(null, `/article/${slug}`)
          }
        >
          {(createDraft, { loading, error }) => (
            <ArticleForm
              loading={loading}
              error={error}
              createDraft={createDraft}
            />
          )}
        </Mutation>
      </Col>
    </Row>
  </BaseLayout>
)

export default DraftLayout
