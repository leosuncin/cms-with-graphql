import PropTypes from 'prop-types'
import { Row, Col, Media } from 'reactstrap'

import CardSearch from '../components/card-search'
import CardArticle from '../components/card-article'
import BaseLayout from './base'

const ProfileLayout = props => (
  <BaseLayout>
    <Row>
      <Col md="8">
        <h1 className="mb-4">{`${props.name.split(' ')[0]}'s profile`}</h1>
        <Media className="mb-4">
          <Media left>
            <Media
              object
              className="mr-3 rounded-circle"
              src={props.image}
              width="128"
              alt={props.name}
            />
          </Media>
          <Media body>
            <Media heading>{props.name}</Media>
            {props.bio}
          </Media>
        </Media>
        <h2>{`${props.name.split(' ')[0]}'s articles`}</h2>
        {props.posts.map(post => (
          <CardArticle key={post.slug} {...post} />
        ))}
      </Col>
      <Col md="4">
        <CardSearch />
      </Col>
    </Row>
  </BaseLayout>
)

ProfileLayout.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string.isRequired,
  bio: PropTypes.string,
  posts: PropTypes.array.isRequired,
}
ProfileLayout.defaultProps = {
  image: 'https://placeholdit.imgix.net/~text?w=128&h=128',
  bio: '',
}

export default ProfileLayout
