# Basic CMS

> The intent of this project is to learn GraphQL and Next.js doing a real and production ready application, but not necessary the best

```bash
cms-with-grapql <- Monorepo
├── docker-compose.yml <- Docker services (development)
├── lerna.json <- Lerna config to manage the monorepo
├── package.json <- Repository dependencies
├── packages
│   ├── backend <- GraphQL server
│   │   ├── backpack.config.js
│   │   ├── Dockerfile
│   │   ├── jest.config.js
│   │   ├── package.json
│   │   ├── package-lock.json
│   │   ├── prisma.yml <- Prisma config
│   │   ├── src <- Source code for backend
│   │   └── __tests__ <- End to end tests
│   └── frontend <- Frontend
│       ├── components <- State components
│       ├── Dockerfile
│       ├── graphql <- GraphQL queries and mutations
│       ├── layout <- Page layouts
│       ├── lib <- HOC
│       ├── next.config.js
│       ├── package.json
│       ├── pages <- Pages
│       ├── README.md
│       ├── routes.js <- Custom routes
│       ├── server.js <- Entrypoint
│       └── static <- Assets
├── README.md
└── yarn.lock
```

## Requirements

- node.js
- docker and docker-compose

## Setup

1. Clone this repository `git https://gitlab.com/leosuncin/cms-with-graphql.git`
2. Install dependencies `yarn install`
3. Create file .env with environmental variables `cp .env.example .env`
4. Customize environmental variables, edit .env file
5. Run containers `docker-compose up -d`
6. Deploy Prisma `(cd packages/backend && yarn db:deploy)`
7. Run the application `yarn dev`
8. Open your browser:

- frontend: http://localhost:3000
- backend: http://localhost:4000
- prisma: http://localhost:4466
- minio: http://localhost:9000
- email: http://localhost:1080
- database: http://localhost:9090

## test

1. Clean up the database `docker-compose down -v && sleep 3 && docker-compose up -d`
2. Restore database and fixtures `(cd packages/backend && yarn db:deploy)`

**Backend:**

```
cd packages/frontend
yarn test:ci # If the backend is stopped
yarn test:e2e # If the backend is running
```

## Demo

https://future-imperfect.suncin.me/
